const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const http = require('http');
const server = http.createServer(app)
const io = require('socket.io')(server);
const fs = require('fs');
const { spawn, exec} = require('child_process');
const readLastLines = require('read-last-lines');
let bodyParser = require('body-parser');
const fse = require('fs-extra');
// const bcryptjs = require('bcryptjs');
let execSync = require('child_process').execSync;
const tcpPortUsed = require('tcp-port-used');
const axios = require("axios");
const CancelToken = axios.CancelToken;
const os = require('os');
let licenseData;
const { spawnSync } = require('child_process');
let forceResetPassword = false;

let addresses = [] ,networkInterfaces = require('os').networkInterfaces();
for (let development in networkInterfaces) {
    const iface = networkInterfaces[development].filter((details) => { return details.family === 'IPv4' && details.internal === false && details.mac !== '00:00:00:00:00:00' });
    if(iface.length > 0) addresses.push(iface[0].address);
}
const validIp = addresses.find(
    ip =>
        !ip.startsWith('10.') &&
        !ip.startsWith('172.16') &&
        !ip.startsWith('172.17') &&
        !ip.startsWith('172.18') &&
        !ip.startsWith('172.19') &&
        !ip.startsWith('172.20') &&
        !ip.startsWith('172.21') &&
        !ip.startsWith('172.22') &&
        !ip.startsWith('172.23') &&
        !ip.startsWith('172.24') &&
        !ip.startsWith('172.25') &&
        !ip.startsWith('172.26') &&
        !ip.startsWith('172.27') &&
        !ip.startsWith('172.28') &&
        !ip.startsWith('172.29') &&
        !ip.startsWith('172.30') &&
        !ip.startsWith('172.31') &&
        !ip.startsWith('192.168')
);
if (validIp) addresses = [validIp];

app.use(express.static(__dirname + '/dist/'));
app.use(bodyParser.json());
let logfilePath = '/var/log/booting/installation.debug.log';
let serverConfPath = `/etc/booting/server_data.conf`;
let serverLicensePath = `/etc/booting/cloud.cnf`;
let userConfPath = `/etc/booting/user_data.conf`;
let bootingConfPath = `/etc/booting/booting_data.conf`;

if (!fs.existsSync(logfilePath)) {
    fse.outputFile(logfilePath, '', err => {
        if(err) console.log(err);
    });
}

io.sockets.on('connection', socket => {
    // console.log('New client connected: ' + socket.handshake.address);

    socket.on('init', async (data, callback) => {

        if (fs.existsSync(serverLicensePath)) {
            await readLastLines.read(serverLicensePath, 1).then((line) => {
                if (line && line.length > 6) {
                    console.log('server license detected, continue')
                    licenseData = line.trim();
                }
            });
        }

        try {
            const result = await spawnSync('chage', ['-l', 'root']);
            const content = result && result.output ? result.output.toString() : '';
            const arrayOfLines = content.match(/[^\r\n]+/g) || [];
            for (let i = 0; i < arrayOfLines.length; i += 1) {
                const line = arrayOfLines[i];
                if (line.includes('Password expires')) {
                    const expires = line.split(':') && line.split(':').length > 0 ? line.split(':')[1].trim() : '';
                    // if ((new Date(expires) && new Date(expires) < new Date()) || expires.includes('password must be changed')) {
                    if ((new Date(expires) && new Date(expires) < new Date()) || expires.includes('password must be changed')) {
                        // console.log('date1: ', new Date(expires), expires, username);
                        forceResetPassword = true;
                    }
                }
            }
        } catch (err) {
            console.log('problem with chage function')
            console.log(err);
            return process.exit(1);
        }


        try {
            let result = execSync('systemctl show mysqld | grep LoadState=loaded');
            result = execSync('systemctl show postgresql | grep LoadState=loaded');
            result = execSync('systemctl show postgresql-10 | grep LoadState=loaded');
            result = execSync('systemctl show postgresql-11 | grep LoadState=loaded');
            result = execSync('systemctl show postgresql-12 | grep LoadState=loaded');
            result = execSync('systemctl show mongod | grep LoadState=loaded');

            result = execSync('systemctl show proftpd | grep LoadState=loaded');
            result = execSync('systemctl show exim | grep LoadState=loaded');
            result = execSync('systemctl show named | grep LoadState=loaded');
            result = execSync('systemctl show elasticsearch | grep LoadState=loaded');

            result = execSync('systemctl show php-fpm56 | grep LoadState=loaded');
            result = execSync('systemctl show php-fpm70 | grep LoadState=loaded');
            result = execSync('systemctl show php-fpm71 | grep LoadState=loaded');
            result = execSync('systemctl show php-fpm72 | grep LoadState=loaded');
            result = execSync('systemctl show php-fpm73 | grep LoadState=loaded');
            result = execSync('systemctl show php-fpm74 | grep LoadState=loaded');

            result = execSync('systemctl show httpd | grep LoadState=loaded');
            result = execSync('systemctl show nginx | grep LoadState=loaded');

            return io.sockets.emit('serverIsNotEmpty', true);
        } catch (err) {
            // console.log('server is empty, continue')
        }


        if (fs.existsSync(serverConfPath)) {
            readLastLines.read(logfilePath, 1).then((line) => {
                if (line.includes('Installation of LAMP server has been completed successfully')) {
                    io.sockets.emit('installEnd', true);
                } else {
                    // console.log('last line on log file: ' + line);

                    let fileSizeInBytes = fs.statSync(logfilePath).size;
                    // console.log(fileSizeInBytes);
                    if (fileSizeInBytes && (fileSizeInBytes === 0)) {
                        io.sockets.emit('serverIsReady', {licenseData, hostname: os.hostname(), forceResetPassword});
                    } else {
                        io.sockets.emit('serverIsBusy', {licenseData, hostname: os.hostname(), forceResetPassword});
                        fs.watchFile(logfilePath, {interval: 700}, (curr, prev) => {
                            readLastLines.read(logfilePath, 20).then((lines) => io.sockets.emit('logChanged', lines));
                        });
                    }
                }
            });
        } else {
            io.sockets.emit('serverIsReady', {licenseData, hostname: os.hostname(), forceResetPassword});
        }
    });

    socket.on('validateHostname', (data, callback) => {
        try {
            if ( !/^(?!:\/\/)([a-zA-Z0-9-*]+\.)*[a-zA-Z0-9][a-zA-Z0-9-]+\.[a-zA-Z]{2,11}?$/.test(data ? data.toLowerCase() : '') ) {
                throw {output : 'Please enter a valid hostname'};
            }
            let result = execSync(`dig +short @1.1.1.1 ${data ? data.toLowerCase() : ''} +tcp`);
            const resultAddress  = result.toString().replace(/\s/g, '');
            const localAddress = addresses[0].replace(/\s/g, '');

            if (!result.toString() || resultAddress != localAddress) {
                throw {output : 'Please make sure that the hostname DNS points to the correct IP address'};
            }
            throw {output : ''};
        } catch (err) {
            // console.log("err: ", err.output.toString());
            callback({
                data: err,
                output: err.output.toString(),
                success: true
            })
        }

    });

    socket.on('getServiceOptions', async (callback) => {
        let errors = [];
        const results = {
            mysqlVersions: [
                { value: '5.6', label: 'MySQL 5.6' },
                { value: '5.7', label: 'MySQL 5.7' },
                { value: '8.0', label: 'MySQL 8.0' },
            ],
            phpVersions: [],
        };

        try {
            const [phpPromise] = await Promise.allSettled([
                new Promise((resolve, reject) => exec("yum -y search php-fpm 2>&1", (err, stdout, stderr) => {
                    if (!!stderr) {
                        reject(stderr);
                        return;
                    }

                    results.phpVersions = stdout.split(/\r\n|\r|\n/)
                        .filter(l => /^php[0-9]{2}/.test(l))
                        .map(l => {
                            const version = l.replace(/^php([0-9])([0-9]).+$/, '$1.$2');
                            return {
                                value: version,
                                label: 'PHP ' + version
                            };
                        });

                    resolve();
                })),
            ]);

            if(phpPromise.status === 'rejected') {
                logger.error(timezonesPromise.reason.toString());
                errors.push('Could not get timezone information from PHP');
            }

            return callback({
                results: results,
                errors: errors,
                success: true
            });

        } catch (err) {
            // console.log("err: ", err.output.toString());
            return callback({
                results: results,
                errors: errors,
                success: true
            });
        }

    });

    socket.on('resetPassword', async (data, callback) => {
        console.log('resetPassword');
        try {
            if ( /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/g.test(data ? data.toLowerCase() : '') ) {
                // throw {output : 'please enter a valid password'};
                return callback({
                    data: 'please enter a valid password',
                    success: false
                })
            }
            await spawnSync('passwd', ['--stdin', 'root'], { input: data });
            callback({
                success: true
            });

        } catch (err) {
            // console.log("err: ", err.output.toString());
            callback({
                data: err,
                output: err.output.toString(),
                success: false
            })
        }

    });

    socket.on('closeInstallation', () => {
        console.log('closeInstallation');
        process.exit()
    });
    socket.on('start_install', (data) => {
        let randomPassword = Math.random().toString(36).slice(-8) + Math.random().toString(36).slice(-8) + Math.random().toString(36).slice(-8);
        // return console.log(data);

        let serverDataText =
            `[PHP]\n` +
            `php_ver1=${data.php}\n` +
            `php_vers=${data.php}\n\n` +
            `[PHP_EXTENSIONS]\n` +
            `imap=no\n` +
            `imagick=no\n\n` +
            `[MYSQL]\n` +
            `mysql=mysql\n` +
            `mysql_ver=${data.mysql}\n\n` +
            `[WEBSERVER]\n` +
            `webserver=${data.server}\n\n` +
            `[SETTINGS]\n` +
            `server_ip=${addresses[0]}\n` +
            `#server_ip_public=\n` +
            `server_ipv6=::\n` +
            `hostname=${data.hostname ? data.hostname.toLowerCase() : os.hostname()}\n\n` +
            `[EXTENSIONS]\n` +
            `node=no\n` +
            `geoip=no\n` +
            `bootingagent=${!!licenseData ? 'yes' : 'no'}\n` +
            `wpcli=no\n` +
            `phpmyadmin=no\n` +
            `react=no\n` +
            `vmtools=no\n` +
            `elasticsearch=no\n` +
            `redis=no\n` +
            `qemuagent=no\n` +
            `mongodb=no\n`;

        let userDataText = `email=${data.email}\n`;

        fse.outputFile(userConfPath, userDataText, {mode: 0o600}, err => {
            if(err) console.log(err);
        });
        fse.outputFile(bootingConfPath, `postgresql_booting_password=${randomPassword}\n`, {mode: 0o600}, err => {
            if(err) console.log(err);
        });
        fse.outputFile(serverConfPath, serverDataText, {mode: 0o600} , err => {
            if(err) console.log(err);
            else {
                console.log('\n\n\n\nInstallation in progress, Please do not close when a task is running.\n')
                console.log('\nInstructions for further operation will appear in the browser, please follow.')
            }

            startInstall(data.licensekey);
        });
    });

    /*socket.on('disconnect', () => {
        console.log('user disconnected')
    })*/
});


// Routes
app.post('/license',async function(req, res) {
    try {
        // console.log('license', req.body.licensekey)
        // console.log(addresses);
        /*if (licenseData) {
            await res.json({success: true, license: {status: 'Active'}});
            return res.end();
        } else {
            const {checkLicense} = require('./license');
            let license = await checkLicense(req.body.licensekey, req.body.hostname ? req.body.hostname.toLowerCase() : '', addresses[0]);
            await res.json({success: true, license: license});
            return res.end();
        }*/
        // let publicIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress;
        let publicIp = await axios.get('https://ipinfo.io/json');
        publicIp = publicIp.data && publicIp.data.ip ? publicIp.data.ip : '';
        publicIp = publicIp ? publicIp.replace('::ffff:', '') : '';

        const {checkLicense} = require('./license');
        let license = await checkLicense(licenseData ? licenseData : req.body.licensekey, req.body.hostname ? req.body.hostname.toLowerCase() : '', addresses[0], publicIp);
        await res.json({success: true, license: license});
        return res.end();

    } catch (err) {
        await res.json({success: false, message: err.message ? err.message : err});
        return res.end();
    }
});
app.use(function(req, res, next) {
    if (req.url === '/license') return next();
    if (req.url !== '/') {
        res.redirect('/');
        return res.end();
    } else res.sendFile(__dirname + '/index.html');
});



function startInstall(license) {
    io.sockets.emit('serverIsBusy', {hostname: os.hostname()});
    fs.watchFile(logfilePath, {interval: 700}, (curr, prev) => {
        readLastLines.read(logfilePath, 20).then((lines) => io.sockets.emit('logChanged', lines));
    });

    let cmd  = spawn('/usr/local/lib/booting-installation/install', ['fullinstall', license]);
    cmd.on('error', function(err) {
        console.error('Error on child process: ' + err);
        io.sockets.emit('debug', { err })
        fse.removeSync(serverConfPath);
    });
    cmd.on('uncaughtException', function (err) {
        console.error(err);
        io.sockets.emit('debug', { err })
        fse.removeSync(serverConfPath);
    });

    let logStream = fs.createWriteStream(logfilePath, {flags: 'a'});
    cmd.stdout.pipe(logStream);
    cmd.stderr.pipe(logStream);

    cmd.on('close', (code) => {
        io.sockets.emit('debug', { code })
        if (code === 0) io.sockets.emit('installEnd', true)
        else {
            io.sockets.emit('general_error', {message: 'Error on process'})
            console.log(`child process exited with code ${code}`);
        }
    });
}


tcpPortUsed.check(PORT).then(
    async inUse => {
        if (inUse) {
            console.log(`Port ${PORT} is already in use.`);
            addresses.forEach(add => {
                const myURL = new URL(`http://${add}:${PORT}/`);
                console.log(`Booting Starter App listening on ${myURL}`);
            });
            return process.exit(1);
        } else {
            server.listen(PORT, () => {
                console.log('\n\n\n\n\n\n**************************************************************************\n')
                addresses.map((add) => {
                    console.log(`Booting Starter App listening on http://${add}:${PORT}/`);
                });
                console.log('File can be run manually by command:\n/usr/local/lib/booting-installation/booting-starter');
                console.log('\n\n')
            });
        }
    }
);
