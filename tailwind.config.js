module.exports = {
  theme: {
    extend: {
      colors: {
        'booting-superlight': '#F4F8FB',
        'booting-lightblue': '#02AEEF'
      },
      borderRadius: {
        'xl': '.875rem',
        '2xl': '1rem',
        '3xl': '1.125rem',
        '4xl': '1.25rem',
        '5xl': '1.5rem'
      },
      borderWidth: {
        '20': '20px',
        '30': '30px',
        '40': '40px',
      },
      height: {
        '15': '3.75rem',
        '1/2': '50%',
        '72': '18rem',
        '80': '20rem',
        '100': '25rem',
        '50vh': '50vh',
        '66vh': '66vh',
        '75vh': '75vh'
      },
      width: {
        '96': '24rem',
        '128': '32rem',
        '256': '64rem',
        'double': '200%'
      },
      minHeight: {
        '1/4': '25%',
        '1/3': '33%',
        '1/2': '50%'
      },
      maxHeight: {
        '1/4': '25%',
        '1/3': '33%',
        '1/2': '50%',
        '11/12': '91.666667%',
        '50vh': '50vh',
        '66vh': '66vh',
        '75vh': '75vh'
      },
      inset: {
        '2': '0.5rem',
        '4': '1rem',
        '6': '1.5rem'
      },
      maxWidth: {
        '1.5/1': '150%',
        '4/5/1': '185%',
        '70vw': '70vw',
        '90vw': '90vw',
        'screen': '100vw'
      },
      minWidth: {
        '1/4': '25%',
        '1/3': '33%',
        '1/2': '50%',
        '40':  '10rem',
        '28':  '7rem'
      },
    }
  },
  plugins: [
    require('tailwindcss-dir')()
  ],
  variants: {
    float: ['responsive', 'direction'],
    margin: ['responsive', 'direction'],
    padding: ['responsive', 'direction'],
    inset: ['responsive', 'direction'],
    opacity: ['responsive', 'hover', 'focus'],
    borderWidth: ['responsive', 'hover', 'focus', 'direction']
  },
};
