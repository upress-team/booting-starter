import React from "react";

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      data-name="Layer 1"
      viewBox="0 0 79 85"
    >
      <path
        fill="#007a33"
        d="M39 42l22 13a3 3 0 01-1 1L41 67a3 3 0 01-2 1 3 3 0 01-1-1L18 56a3 3 0 01-1-1z"
      />
      <path
        fill="#215732"
        d="M39 42V17a3 3 0 012 0l19 12a3 3 0 011 1 3 3 0 011 1v23a3 3 0 01-1 1l-15-9h-1z"
      />
      <path
        fill="#009639"
        d="M39 42V17a3 3 0 00-1 0L18 29a3 3 0 00-1 1 2 2 0 000 1v23a3 3 0 000 1z"
      />
      <path
        fill="#fff"
        d="M33 51a3 3 0 01-5 0V34a3 3 0 013-3 5 5 0 013 2l1 1 11 12V34a3 3 0 015 0v17a3 3 0 01-3 3 5 5 0 01-4-2L33 38v13z"
      />
    </svg>
  );
}

export default Icon;
