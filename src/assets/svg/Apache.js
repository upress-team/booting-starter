import React from "react";

function Icon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 2393 4227">
      <linearGradient
        id="APa"
        x1="-5167"
        x2="-4570"
        y1="698"
        y2="1396"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#f69923" />
        <stop offset="0" stopColor="#f79a23" />
        <stop offset="1" stopColor="#e97826" />
      </linearGradient>
      <path
        fill="url(#APa)"
        d="M1799 20c-66 39-176 150-308 311l121 228c85-122 171-231 258-324l10-11-10 11c-29 31-114 130-243 328 124-6 315-31 470-58 46-259-45-377-45-377S1936-61 1799 20z"
      />
      <path
        fill="none"
        d="M1594 1321l3-1-17 2-3 2 17-3zm-123 408zm-838 916l4-10a18591 18591 0 01346-845 13230 13230 0 01182-392l13-28 2-4-14 1-12-23-4 7a9455 9455 0 00-196 412 13152 13152 0 00-270 634 17294 17294 0 00-168 433l19 37 16-2 2-5 80-215zm800-909z"
      />
      <path
        fill="#BE202E"
        d="M1393 1935l-47 8h-1a882 882 0 0048-8zm41-199h-1l8-1 30-6-37 7z"
      />
      <linearGradient
        id="APb"
        x1="-9585"
        x2="-5326"
        y1="621"
        y2="621"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#9e2064" />
        <stop offset="1" stopColor="#c92037" />
        <stop offset="1" stopColor="#cd2335" />
        <stop offset="1" stopColor="#e97826" />
      </linearGradient>
      <path
        fill="url(#APb)"
        d="M1256 1148a7375 7375 0 01230-398l7-12c39-62 79-122 119-180l-121-227-27 33a5419 5419 0 00-237 318 7255 7255 0 00-227 345l-3 7 157 310c33-66 67-132 102-196z"
      />
      <linearGradient
        id="APc"
        x1="-9071"
        x2="-6533"
        y1="1048"
        y2="1048"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#282662" />
        <stop offset="0" stopColor="#662e8d" />
        <stop offset="1" stopColor="#9f2064" />
        <stop offset="1" stopColor="#cd2032" />
      </linearGradient>
      <path
        fill="url(#APc)"
        d="M540 2897l-63 175-1 3-9 25-54 158c46 21 83 77 118 140-3-65-30-127-82-174 229 10 425-48 527-215 9-14 17-30 25-47-46 59-104 84-211 78h-1 1a613 613 0 00308-253c17-27 33-56 50-88a498 498 0 01-469 152l-127 14-12 32z"
      />
      <linearGradient
        id="APd"
        x1="-9346"
        x2="-5087"
        y1="581"
        y2="581"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#9e2064" />
        <stop offset="1" stopColor="#c92037" />
        <stop offset="1" stopColor="#cd2335" />
        <stop offset="1" stopColor="#e97826" />
      </linearGradient>
      <path
        fill="url(#APd)"
        d="M599 2612a21098 21098 0 01260-635 14077 14077 0 01291-626l4-7-157-311-8 13a7346 7346 0 00-318 577 4591 4591 0 00-221 544 3905 3905 0 00-92 351l158 312a22072 22072 0 0183-218z"
      />
      <linearGradient
        id="APe"
        x1="-9036"
        x2="-6797"
        y1="638"
        y2="638"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#282662" />
        <stop offset="0" stopColor="#662e8d" />
        <stop offset="1" stopColor="#9f2064" />
        <stop offset="1" stopColor="#cd2032" />
      </linearGradient>
      <path
        fill="url(#APe)"
        d="M356 2529c-20 100-34 199-41 298l-1 11a626 626 0 00-181-156c95 137 167 273 177 407-50 10-120-5-200-34 84 76 146 97 171 103-77 5-157 58-237 118 117-48 213-67 281-51-108 306-217 643-325 1002 33-10 53-32 64-63 19-65 148-490 348-1050l18-48 4-13 66-180 16-42v-1l-158-312-2 11z"
      />
      <linearGradient
        id="APf"
        x1="-9346"
        x2="-5087"
        y1="1022"
        y2="1022"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#9e2064" />
        <stop offset="1" stopColor="#c92037" />
        <stop offset="1" stopColor="#cd2335" />
        <stop offset="1" stopColor="#e97826" />
      </linearGradient>
      <path
        fill="url(#APf)"
        d="M1178 1370l-14 28a18967 18967 0 00-87 184l-23 50a13230 13230 0 00-251 581 17771 17771 0 00-250 647l-2 5 128-14-8-1c152-19 354-133 485-273 60-65 114-141 165-230 37-66 73-140 106-221 30-72 58-149 85-232a461 461 0 01-167 49h1a554 554 0 00296-288 505 505 0 01-201 80l-8 1h1a577 577 0 00180-119 411 411 0 0070-94 2285 2285 0 0062-133 1005 1005 0 0043-141l-16 12c-43 25-117 48-176 59l117-13-117 13-3 1-18 3 4-2-400 44-2 4z"
      />
      <linearGradient
        id="APg"
        x1="-9610"
        x2="-5351"
        y1="1000"
        y2="1000"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#9e2064" />
        <stop offset="1" stopColor="#c92037" />
        <stop offset="1" stopColor="#cd2335" />
        <stop offset="1" stopColor="#e97826" />
      </linearGradient>
      <path
        fill="url(#APg)"
        d="M1628 563c-36 55-75 117-116 187l-7 11a7207 7207 0 00-223 403l-102 202 400-44a486 486 0 00219-172 1979 1979 0 00157-265c35-68 65-135 89-195a1027 1027 0 0052-185c-155 27-345 52-469 58z"
      />
      <path fill="#BE202E" d="M1370 1939l-25 4 25-4z" />
      <linearGradient
        id="APh"
        x1="-9346"
        x2="-5087"
        y1="1153"
        y2="1153"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#9e2064" />
        <stop offset="1" stopColor="#c92037" />
        <stop offset="1" stopColor="#cd2335" />
        <stop offset="1" stopColor="#e97826" />
      </linearGradient>
      <path fill="url(#APh)" d="M1370 1939l-25 4 25-4z" />
      <path fill="#BE202E" d="M1433 1736l8-1-8 1z" />
      <linearGradient
        id="APi"
        x1="-9346"
        x2="-5087"
        y1="1138"
        y2="1138"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#9e2064" />
        <stop offset="1" stopColor="#c92037" />
        <stop offset="1" stopColor="#cd2335" />
        <stop offset="1" stopColor="#e97826" />
      </linearGradient>
      <path fill="url(#APi)" d="M1433 1736l8-1-8 1z" />
      <path fill="#BE202E" d="M1434 1736v-1 1z" />
      <linearGradient
        id="APj"
        x1="-6953"
        x2="-6012"
        y1="1135"
        y2="1135"
        gradientTransform="rotate(-65 -2053 -4778)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset="0" stopColor="#9e2064" />
        <stop offset="1" stopColor="#c92037" />
        <stop offset="1" stopColor="#cd2335" />
        <stop offset="1" stopColor="#e97826" />
      </linearGradient>
      <path fill="url(#APj)" d="M1434 1736v-1 1z" />
      <path
        fill="#6D6E71"
        d="M2219 387v12h27v77h13v-77h27v-12h-67zm158 0l-31 62-30-62h-16v89h12v-71l30 62h8l31-62v71h11v-89h-15z"
      />
    </svg>
  );
}

export default Icon;
