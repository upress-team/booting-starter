import React, { Component } from 'react';

class Loader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {

        return  (
            <div className='spinner_overlay dir-ltr animated fadeIn'>
                <div className='spinner' style={{marginTop: this.props.title ? '-1em' : ''}}>
                    {this.props.title && <span className='title'>{this.props.title}</span>}
                </div>
            </div>
        );
    }
}

export default Loader;