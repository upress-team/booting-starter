import React, {Component} from 'react';
import './assets/App.css'
import './assets/animate.css'
import Loader from "./assets/Loader";
import socketIOClient from 'socket.io-client';
import ScreenWelcome from "./screens/ScreenWelcome";
import ScreenSelectServerPurpose from "./screens/ScreenSelectServerPurpose";
import ScreenSelectServertype from "./screens/ScreenSelectServerType";
import ScreenServerSettings from "./screens/ScreenServerSettings";
import ScreenUserSettings from "./screens/ScreenUserSettings";
import ScreenSummary from "./screens/ScreenSummary";
import ScreenInstallRunning from "./screens/ScreenInstallRunning";
import ScreenComplete from "./screens/ScreenComplete";
import Clip5 from './assets/svg/Clip5';
import ClipWaiting from './assets/svg/ClipWaiting'
import ScreenResetPassword from "./screens/ScreenResetPassword";
const axios = require('axios');
var pjson = require('../package');

// const isDev = window.location.hostname === 'localhost';
// const socketEndpoint =  isDev ? "http://localhost:3000" : "/";
const socketEndpoint = '/';
// console.log(socketEndpoint);

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isSocketConnectionError: false,
            isGeneralError: false,
            message: null,
            php_version: "7.2",
            mysql_version: "5.6",
            buffer: '',
            hideLicense: false,
            screen: -1,
            server_purpose: 'stand_alone',
            server_type: null,
            isInstallRunning: false,
            isInstallComplete: false,
            email: null,
            username: null,
            password: null,
            confirm_password: null,
            hostname: null,
            isBusy: false,
            forceResetPassword: false,
            isLocalHostname: false
        };
        this.screens = [
            // () => <ScreenSelectServerPurpose confirmButtonAction={(val) => this.nextScreen.bind(this, {'server_purpose': val} )} />,
            () => <ScreenResetPassword confirmButtonAction={() => this.nextScreen({'forceResetPassword': false} )} />,
            () => <ScreenSelectServertype previusScreen={this.previusScreen.bind(this)} confirmButtonAction={(val) => this.nextScreen.bind(this, {'server_type': val} )} server_purpose={this.state.server_purpose} />,
            () => <ScreenServerSettings previusScreen={(val) => this.previusScreen.call(this, val)} state={this.state} confirmButtonAction={(val) => this.nextScreen.call(this, val)} server_purpose={this.state.server_purpose} server_type={this.state.server_type} php_version={this.state.php_version} mysql_version={this.state.mysql_version} hostname={this.state.hostname} isLocalHostname={this.state.isLocalHostname} handleIsLocalHostnameChange={this.handleIsLocalHostnameChange.bind(this)}/>,
            // () => <ScreenUserSettings previusScreen={(val) => this.previusScreen.call(this, val)} confirmButtonAction={(val) => this.nextScreen.call(this, val)} server_purpose={this.state.server_purpose} server_type={this.state.server_type} username={this.state.username} password={this.state.password} confirm_password={this.state.confirm_password} email={this.state.email} />,
            () => <ScreenSummary previusScreen={this.previusScreen.bind(this)} confirmButtonAction={this.onSubmit.bind(this)} state={this.state} />
        ];
        this.socket = null
    }

    nextScreen(val) {
        // console.log(this.state.screen);
        if (this.state.isBusy) return;
        if(val) {
            this.setState({...val, ...{screen: this.state.screen+1}})
        }
        else this.setState({screen: this.state.screen+1})
    }
    previusScreen(val) {
        if (this.state.isBusy) return;
        if(val) {
            this.setState({...val, ...{screen: this.state.screen-1}})
        }
        else this.setState({screen: this.state.screen-1})
    }
    async doApiAction(url, method, params={}, options={}, validateUid = true) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        let result = await axios({
            method: method,
            url: url,
            data: params,
            headers: headers,
            options: options
        });
        if(validateUid && result.headers && result.headers['content-type'] && result.headers['content-type'] === 'text/html; charset=UTF-8') window.location.replace('/login');
        return result;
    }

    handleIsLocalHostnameChange(e) {
        this.setState({isLocalHostname : !this.state.isLocalHostname});
    }

    onSubmit(licensekey) {
        if (licensekey === 'trial') {
            this.setState({isInstallRunning: true}, () => {
                this.socket.emit('start_install', {
                    php: this.state.php_version,
                    mysql: this.state.mysql_version,
                    server: this.state.server_type,
                    email: this.state.email,
                    licensekey: licensekey,
                    hostname: this.state.hostname,
                });
            })
        } else {
            this.setState({isBusy: true}, async () => {
                // return console.log(licensekey);
                this.doApiAction(`/license`, 'POST', {
                    licensekey: licensekey,
                    hostname: this.state.hostname,
                }).then((response) => {
                    this.setState({isBusy: false});
                    // console.log(response.data);
                    // response.data.license.status = 'Active';
                    if (response.data.license && response.data.license.status === 'Active') {
                        this.setState({isInstallRunning: true}, () => {
                            this.socket.emit('start_install', {
                                php: this.state.php_version,
                                mysql: this.state.mysql_version,
                                server: this.state.server_type,
                                email: this.state.email,
                                licensekey: licensekey,
                                hostname: this.state.hostname,
                            });
                        })
                    } else this.setState({message: response.data.description ? response.data.description : 'Please make sure the license key is correct and active'})
                }).catch((err) => this.setState({isBusy: false, message: err.message ? err.message : 'License - General Error'}))
            });
        }
    }



    componentDidMount() {
        setTimeout(
            () => {
                this.setState({isLoading: true}, () => {
                    this.socket = socketIOClient(socketEndpoint);
                    this.socket.emit('init', true);
                    this.socket.on('connect_failed', () => {
                        if (!this.state.isInstallComplete) this.setState({isSocketConnectionError: true})
                    });
                    this.socket.on('connect_error', () => {
                        if (!this.state.isInstallComplete)
                        this.setState({isSocketConnectionError: true})
                    });
                    this.socket.on('general_error', (error) => {
                        this.setState({isGeneralError: true, message: error ? error.message : null})
                    });
                    this.socket.on('debug', (msg) => {
                        // console.log('debug', msg);
                    });


                    this.socket.on('logChanged', (lines) => {
                        // console.log(lines);
                        this.setState({buffer: lines}, () => {
                            let buffer = document.getElementById('buffer');
                            if (buffer) buffer.scrollTop = buffer.scrollHeight;
                        });
                    });
                    this.socket.on('installEnd', () => {
                        this.setState({screen: 0, isInstallComplete: true, isInstallRunning: false, isLoading: false, isSocketConnectionError: false});
                    });
                    this.socket.on('serverIsReady', (data) => {
                        this.setState({screen: data && data.forceResetPassword ? 0 : 1, isLoading: false, isSocketConnectionError: false, hideLicense: data && data.licenseData ? data.licenseData : false, hostname: data && data.hostname ? data.hostname : ''})
                    });
                    this.socket.on('serverIsBusy', (data) => {
                        this.setState({screen: data && data.forceResetPassword ? 0 : 1, isInstallRunning: true, isLoading: false, isSocketConnectionError: false, hideLicense: data && data.licenseData ? data.licenseData : false, hostname: data && data.hostname ? data.hostname : ''})
                    });
                });
                // this.setState({screen: 0, isLoading: false, isSocketConnectionError: false})
            },
            5000
        );
    }
    getStepText(step) {
        let text = '';
        switch (step) {
            case 1:
                text = "password";
                break;
            case 2:
                text = "web server";
                break;
            case 3:
                text = "configuration";
                break;
            case 4:
                text = "summary";
                break;
            /*case 5:
                text = "summary";
                break;*/
        }
        return text;
    }

    trans(slug) {
        let text = slug;
        switch (slug) {
            case 'server_purpose':
                text = "purpose";
                break;
            case 'server_type':
                text = "type";
                break;
            case 'php_version':
                text = "php version";
                break;
            case 'mysql_version':
                text = "mysql version";
                break;
            case 'stand_alone':
                text = 'stand alone';

        }
        return text;
    }

    render() {
        if (this.state.isSocketConnectionError) return <Loader title={'Connection Error - Please Wait'}/>
        if (this.state.isGeneralError) return <div className={'absolute bottom-0 flex items-center justify-center left-0 right-0 top-0'}>{this.state.message ? this.state.message : 'General Error'}</div>
        if (this.state.isLoading) return <Loader/>
        if (this.state.screen === -1) return <ScreenWelcome />
        return (
            <div className={''}>

                <div className='fixed flex bottom-0 top-0 right-0 left-0 justify-center p-6'>
                    <div className='flex flex-col w-full md:w-4/5 overflow-x-hidden overflow-y-auto px-4 max-w-3xl'>

                        <div className='content flex-grow'>
                            <div className="flex cursor-pointer items-center flex-shrink-0 text-center h-20 mb-10 justify-center">
                                <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAHqCAMAAAAd2OhZAAABL1BMVEVHcEwAru8Aru8SkscAru9/wkG7jEYAru8Aru8Aru8Aru8Aru8Aru8Aru8Aru8Aru8Aru8Aru8Aru8Aru8Aru8Aru8Go+EAru8Aru8Aru8Aru/pcigAru8oRWoAru8Aru8oRWooRWrxyREoRWrxyRH2hB/0pRjkHTh/wkEoRWooRWrkHTj2hB/xyRH2hB/kHTgoRWrxyREoRWooRWr2hB/kHTgoRWrkHTjkHTjkHTjxyRF/wkHkHTgoRWrkHTjkHTjXtSLkHTjkHTjybyT2hB/2hB/xyRHxyRHxyRH2hB/xyRHayBz2hB9/wkH2hB9/wkH2hB/xyRH2hB/xyRHkHTh/wkH2hB/xyRH2hB/kHTh/wkEWfLD2hB/xyRF/wkEAru/xyRHkHTgoRWp/wkH2hB+CMEI8AAAAX3RSTlMAGygI8okEnNhl+U+CuKfLwbBuQ+DoD1yV0zIMO5eMd0BWp7B3pyM+NN1oT+TWfF8cjsZ91vHxoi605VPfL5IcHG6AFZBKZvK/vjAUMWny8j5MYz3DylhZcNLjKvxFrpUzDfgAABIKSURBVHja7d3ZVhTJFgbgrKGzqHmeAKoABQUVUNF2ZBbFRkWcsBens8B6/2c44Nw0UBGREZF7R/3/xTlr9QW5Or/OGPaOzPJaQdJIgqaHqCQVGEoa9xYgAAEIQAACEIAABCAAQQACEAQgAAEIQAACEIAABCAAQQACEAQgAAEIQAACEIAABCAAQQACEAQgAAEIQAACEIAABCAAQQACEAQgAAEIQAACEIAABCAAQQACEAQgAAEIQAACEIAABCAAQQACEAQgAAEIQAACEIAABCAAQQACEAQgAAEIQAACEIAABCAAQQACEAQgAAEIQAACEIAABCAAAQhAAIIABCAAAQhAAAIQgAAEIAABCEDCJRGLj1azzUo31+mkTlLIF07/r9PJdSvNbHU03gCIDZBEvNrstsqlkXpmwNXbADEK4sezlVa+nRS++ghATIHEqpVUqS57dYCYAGlUu4URtasDRDdIrNkqZtSvDhCdILFmqhby6gDRBZLIttoarg4QLSCxSj6j5+oACQ8ymmvruzpAQoKMdmparw6QMCDxXE331QGiDNKotA1cHSCKINm8masDRAUk1qmbujpA5EGq+cBcACIJkqjUggAgVEBirUwQAIQKyGg5MB6ACIOMl4IAIGRAqsUgAAgZkOpIEACEDIitp0MjyOq1G4uLNx6MuQgyapFDD8i1lc9H33Lv/fqqYyCxfBDwAll8cvSvPHzgEEijFQS8QK6d4TjNn74rIJVMwAxk4+i83HvgBEi1FgTMQB4eXZAb/EFsTx46QC70sCFiGCQXBOxAVo4uyTXWIOO1gB/I4mUeR0/G+IIkCkHAD2T13qUgRytcQbLxZMARZOVoQB4wBallAo4gDwZ5HP3JFCTaKIO8HwhytAoQeyCLgz2OFgFiDcR/IgDyECDWQDYEPI6e+ACxA+IPWvLamEQA8lv+FPIwu1sHyG81dzEPswUtgEgteQFiEWRR0ANDlh2QsSeiIA8AYgNkQ9Tj3hhALIA8EPU4eo+NoQ2Qh8IgGwCxAHJD2MNw/R0g3/KZyIgFkG9ZF39AbgDEPMjmPWEPs7VegAj2be0dOwGI1JLXdAMXIJJL3nurADEPIrHkXfcAYh5EfMn72QOIeZANMktegIgcVbS45AWIRN/WwqFFgMj0bS0c6wWITN/WdB8EIJJ9WxtLXoCIHVW0tuQFCLEl79CD+KuEilgAobfkHXYQX2LJu+EBxPwTIl7EeuIDxDyIRN920QOIcZCxe1QONgCEWt8WIMT6tgAh1rcFiFTfdsMDiHmQz9SKWEMOsk6uiDXcIMT6tgBZoVfEGmoQan3boQeRWPJuAsQ8yA1qfdthB3lPdMk7rCDXqC55hxVkheqSd1hBPlNd8g4pyCrNItbQgoh30p+MAcTGE7JIr2873CDr9Pq2zoBkkrV2qZQvnyZfKrZryYw+EE19283dvb3dTcdBku1yq9usxmOJM/86idhotdltFfPhhywdfdux/Znlw/5JDpdn9sfcBKmVc83RRpi7JFg4Cd+3nZ3p/57DmVnHQDKlTjYW/j/bVTtL3rF/c5yK9Ld8d0DanWxD07z52cZRxYPD/jm5uesESCZfiWlcyKyYX/I+u9m/IB/5g+TTDb0ryxumi1izO/2Ls88bpF3RrCE2ZoUpYvlb/UvzjC9IshU3svlaNNm33X99uUf/9SZTkJF0wtR2+LOxvu3ecn9gZliClKoG6xPXDM3omzt9kTzjB5KPe0azYWSPvtU/FALZ4QZSNszhXfp2oWpR8ePrvmj2WIGUzHNcJvJebUu4uyzM0T/cYgQyMm6pzr2hc7zanOnLZJkNSLJpr/Nw3k90K87nC33JzDIBafmezayfPXK9otS0vbhOorDOogTSjnuW4y8+/GXyfl2JY3enL599DiDdSFqmY9fWN1ZWNtavqZ1oGJvpq2SBPkgx5kUY31MbLA8O+46CdD2GUZg8mIDURhlyzO70lfORNkiBIcegIrvqVp0CSJqhx/7rMB6vxwiD1BkOV3vL/VDZIbxTLzbYcWzu9EPmgC5Iit/jIVpkV+sZprDaNVZkvzBbZPshTW4cu8vhOfqv16iCVLlNHjN9HTkg2lPPjDPzWNDCcfkZhwhBksyWu3s39Xgs+zRB6nFeHlt6OPo3BxwxigokycxjRpPHzqCqckQgmSH1WPCIgvCaP3xN0/mOwBs7Kax3BfoeemaPZyLXigSE235Qx27wcEHsWlGAVIZw/zEj2rKPAKTDrRUVegNyuLwrfDX7IGV25cTQtauPElezDjKS4AYyE3qp6xEG4bYBCT1izUi+/JPCAmtQxT1U4WpX9nIpTOjmppDX+/KXswtS5OfhHag3BlVOQ6YwgRjaheysKV3OKgjHA1iKIDf3FC9nE4TjAUW1IevwQPlyFkHqMZYgCpP6VoiPNaaw4tW97BUpslMAKfP0kN0YihXZCYBkYkxBpEonhwthr2YNpMLVQ6Y9NRP+S78pbAm19afk6yQRglQZgxzoL7JHDlLwPMcfkQVN17IEEmcNsqe9yB41SMfjnQULk4dNkGSMOcilB0lViuwRg+Q8z12RLb3fZ7EBUm/wB/EPtBbZowVx4AHxzn95SrnIHikI/xnkR933TFXr5r6Bi6SwxJJZ/878NLk588zIJVLYg8hl9tn+wsL+s1lTfz+FTbrSHO/5bEHGPYQSSBH3mBZIGveYFIgLm0KnQFK4xbRAqrjFpEDauMO0QLq4w7RARnGHSYFgE0IMJIcbTAsEZRNaICO4v7RAWri/tECyuL+kQDIx3F9SICXcXlogHdxeWiCYQmiBYAohBoJKLzEQ9KaIgVRwd2mBoFlICwRzOjGQ2nDcQP/641d/KOfV4zVrIOVh4Hj8z5svIfP3H9ftgAxBqffV31+05MN1GyDOL7Kua+I4zT8J8yCuF05efdGZN9eNgzh+4OSPL5rz+NvfLRgDicFDRSRv7JR1AuOVZL6OWiUcyVKZz014fPlw+qeLaBcq5IMRkC9/eJ43gn2hwnbwi6GseV4NxXcyD8jXR8QYSAcziEIZxfeSONZLYcn7a+lrDKSLEUtpzDIG4m4pa+2NOZAP5kDSmEJUJhGAyHekHhsEeQMQImUTgAAE+/RoQZxdZfnXeYK4uw9huuzFxlBtY1hHLYtW6cRYcdHdU0AGJ5G/E+b6IQUPY5ZK+b1tCiSPBpVSg8pYC9fpnrrBFq6xUydOn7U2eMihjLcRyJRPvh4DMndQLu4yiLmDci28QEVE5PtR0pwxEMc/2OsbOmxdMQbi/MfLtL6O8PMXKZsBdobqU7uBF3aqATYiYbaIel5p++2nFkaNgSSH4yPj/vUQ73yevPR5/cwPX8TMvaeOT8QKcP7nnzSM1d+5/qB91DFWXcTnstRi7I0dfFBOLebe+nT7pTZjMbdVx2eUlZI2B4KfRlCJuZ3hMOzVDSQeYBIhlUQdk8iwbETwaxVKMdczxE5EKR1zIAG+8qeQpkEQ/MynQsYNgpRxe+XTSJoDwZdJiS2zMGYRW2ZhzFJJN8CYRSpZgyD4ALxCYhmDIPitT2KzOj7JT2xWx6+IKKQSYFofmr06+obE9upBUPNxh2VTMvqIuLVbn327ffXq9ttZoxfJGQVxaOX7bvpR73seTf+P6dbQnd9J+PRT47vJtrGtodFJxJHG4ctbvf/k1juWk4gTj8jV3rl5scZwEnFgFvFv9y7KbROryKpZEP5vJlzs0es9/2RgJ1I3CzLi5nj1cyp5qf2KZcOPCO/t+rveoEz/xamcFQRBknVF61ZvcK7qvWTcMAjr75lt90Ty6K3WixZNi4y7/YB8XQLrnEo6pkH47g7f9oQzPcZm4cu4uz7dk4i2qcSvmQbJMP1ek/9IBqT3SFfNMWX8EWH6FcaXPcm80FOazwYYtM6v8fakc1vHVJKoBxi0FHbp51dTtlmMWTxXWiogWgrzFsYslu+4qYHoKMzXLIhkhwckfGG+YwGkzq+m9UkZJGxhftwCCMNp5GUvRMIV5ts2RLhVGX3JjaHOwnzXBgi/Y1rTvXBRrqb4MSsg7M7DfwoJEqIwX7YCkmS2Pww5ZoUpzDftPCI1Zt8rvdoLH7XCfKJmR4TZsSD/lgYRtakkZweEW+H3rQ4QpcJ83BIIt9elb2sRUSnMl22JMPva3LQeEflxK2sLhNu7h7pEXsjWt9oQMTpq9W5JLrcq1kC4zSP/e6RJRO4ZMfu+Ieu1ln9Vj8g0vSL8z/0Irx2i/5eemUSuvxu3CBLUmLVH/Jc69ojP5XqJKZsiGXYnTD89Dy9yW+qKo4HVsKvG++HXW8/leiQFuyL8zsWvvbA7i4zbBeE2tZ/mXcip5AXR+smPBgnDVxW2Q00lz+V2h9XAdvid1/LHQk0lksfo8tZFivyOB/mzIaYSyfNB44H9cHxzWr2aIlv1LUQgkuf4Q5RXrexErO9FotySJCbn705M3J2fVPvvQbGaIt0XSUUiUrR+HmXqyv3jH7kzr/SLQErVFOnXFeJBNGlZ/ZWk+V8a3zKhdPm38ktg+ZNanYhEkvbGrcm54/9mys5UIt9cb9QjEglqll5ZuHt8biZsFOZvKVyjEkSWoo3DphPHF+SO2lsdUlPJbZUrtIMIScYj8zgRUXpGpArzSu+7ZYMo085GMV6FGLVkCvMv1P5+OVKRoF4xt+KaPL4084YL84qn4eNBxMmkTO1L7lwOcn/JaGF+WvWPd4LIU0ybKKjMHw/IhPKf9gcX5iVb6r//8Vr0IkGmkNU+dM0NAjl+GuKvDyrMh/iuVjYgkWQhq/M58ScHehxfMVeYD/WRh0JAJJl8Rd98MjEYRHHH/qua8khTJ+RMYsmATmqpdNzSiHV8PBf2a7znV1Oeh/3iRjqglXq5mw3bXFw6FsndsOznVVM0fHQ5H9BLvdTqZkcvdomNptPhppDTLIW+d7NnSKZ1fEgrngmIJlMv5gutXCXdzJ7k5H+a6UquVcgX65nLv9w8LwYyoeHu/bX94/ctbk1va/q0byXgGA0gx5N6buDay3fvXur8znJ+WEHueDRDd9BSBJkSBFEvaRlO2jGQSVGQuQRRkYJbIEuiIOGXvoYSXTvXCMigWq+mkpbJVN0CmRAGuUIUxNonN+yACE8i2pa++lNyCURizKK69KVVZQwNMiX+iFBd+lJpjegBkXhE7jcwjVgAkZhFJqiCRH0IRSuIxEKL7NLX2ufmrIAs3ee/9I3opREzIJeflNPazTWYpkMgQn3c7yUtsiCMJvbBIBJL37t0RQrugHhXxJe+S3RFiu6APHVh6es1as6AyCx9J+mKMOkfioA07vMvabEpxQv9Xvi8C0tfJotfsR9wFy9pzfmERbrOgEw6sfQl8d6IHhCJpe/xEmWRlCsg4ucdCJe0WGwQBUH8u04sfRnU4gVBvMScE0tfj/wBU1EQmaXvPG2RkhsgMt3cBEQsgDjRzaUvIg7iRDeX/jwiDuI70c0lv9aSeELc6OZS34/IgLjRzf2elgsgU84sfenWtaRAHOnmfk/FAZCn7ix9qfZH5EBcWvoS7SFKgri09D3ts9e5g7jSzf15XKDIHUSipMXhEaG3IZEGmXJqFvHInTKVBpFY+t5lAULsFSt5kKeudKp+Te011iDiJa37XH7oJJFnDdKYc2sSoTWRKICIL30n2YB41QxjEOGl7xQfEK9RYgwy6SAIlWFLCUR06TvJCsQbT7IFETzIuMQLxPPLXEHElr6kT8JTrcgrgvhzzhSz/p1YiSeIUElr3uOYNE8QkXl9iSVIxA+JMsikkyPW94ckwxBkcDf3KVsQr1FmCDKomzvhcU61zg5kQElrLuHxTo4dyOWD1qTHPbE8N5DLVlpTngOp1piBXCzihMdJKkleIBeMWvefeq4k0eEFcu5Pe19JeA4lVmAF4nnzZ0iuPPUcS7zMCsTzJid+mNy/c3fJczDxPCuQ013i5NRJniY8V2ONZMRDSA1cAJEgSQGEWtGxkwQIsX1JugYQagWVPECo7RVzdYAQSzYPEGqPSXcEIMQy2qkDhNoMn0oChFb8aqsOEGIZz7UBQm2OT5eTACG2ix/PlTIAIVbtquZKSYBQe1IqhRGAUJtTqt1COwMQYgPYeDNXaCcBQmwIi1fTuUJpJAkQYjCx0WyzkmsV8qXiSK2e/D31WrtYyhdanSbXf7v/AwQB+et3aLz3AAAAAElFTkSuQmCC"} className={'w-16'}/>
                            </div>

                            {!this.state.isInstallRunning && !this.state.isInstallComplete && <div className='flex justify-center items-center mb-4 border p-4 rounded-lg'>
                                {this.screens && this.screens.map((screen, index) => {
                                    return (
                                        <React.Fragment key={index}>
                                                <div className={'flex items-center'}>
                                                    <div className={'flex items-center flex-col'}>
                                                        <div className={'text-xs capitalize mb-2 ' + (this.state.screen >= index ? 'text-booting-lightblue': 'text-gray-400')}>{this.getStepText(index+1)}</div>
                                                        <button type={'button'} disabled={this.state.isBusy} onClick={() => this.state.screen > index && index !== 0 ? this.setState({screen: index}) : {}} className={(this.state.screen > index && index !== 0 ? 'cursor-pointer ' : 'cursor-default ') + 'flex font-semibold rounded-full border mx-1 w-12 h-12 justify-center items-center text-lg ' + (this.state.screen >= index ? 'text-booting-lightblue border-booting-lightblue': 'text-gray-400 border-gray-400')}>
                                                            {index+1}
                                                        </button>
                                                    </div>
                                                    {this.screens.length > (index+1) && <div className={'border-t h-px mt-6 spacer w-12 ' + (this.state.screen > index ? 'border-booting-lightblue' : 'border-gray-400')} />}
                                                </div>
                                        </React.Fragment>
                                    )
                                })}
                            </div>}
                            <div className='flex flex-col justify-center items-center'>
                                {!this.state.isInstallRunning && !this.state.isInstallComplete && this.screens[this.state.screen]()}
                                {this.state.isInstallRunning && <ScreenInstallRunning buffer={this.state.buffer} />}
                                {this.state.isInstallComplete && <ScreenComplete hostname={this.state.hostname} socket={this.socket} isLocalHostname={this.state.isLocalHostname}  />}
                            </div>
                        </div>

                        <footer>
                            <div className='flex text-xs'>
                                <p className="text-gray-500 mx-2">
                                    &copy;2019 Booting. all rights reserved.
                                </p>
                                <a target={'_blank'} href={'https://booting.me/tos'} className='btn-link mx-2'>Terms & Conditions</a>
                                <a target={'_blank'} href={'https://booting.me/policy-privacy'} className='btn-link mx-2'>Policy Privacy</a>
                                <a target={'_blank'} href={'https://booting.me/help'} className='btn-link mx-2'>Help</a>
                                <div className={'ml-auto mr-4 text-booting-lightblue'}>v{pjson.version}</div>
                            </div>
                        </footer>

                    </div>
                    {/*<div className='relative hidden md:block w-1/4' style={{backgroundImage: 'linear-gradient(to top, #3F4D66, #39465f, #4d6184, #596e94, #7f94b9)'}}>
                        <div className='absolute bottom-0 clipicon right-0 w-full z-10'>
                            <div style={{width: '200%', position: 'absolute', bottom: '100%', right: '-20%'}}>{this.state.isInstallRunning ? <ClipWaiting /> : <Clip5/>}</div>
                            {!this.state.isInstallRunning && !this.state.isInstallComplete && <div className='font-semibold ml-4 text-5xl text-center' style={{color: 'rgb(31, 35, 43)'}}>
                                <span className='hidden md:inline-block'>Step </span>
                                <span style={{fontSize: '3em'}}>{this.state.screen+1}</span>
                            </div>}
                        </div>
                    </div>*/}
                    {/* <div className='relative hidden md:block w-1/4 px-4'>
                        <div className="h-20 mb-10 flex" />
                        <div className={'bg-gray-200 text-gray-600 p-4 rounded-lg border py-8'}>
                            <h3 className={'font-bold mb-2'}>Details</h3>
                            <div className={'flex flex-col'}>
                                {['server_purpose', 'server_type', 'php_version', 'mysql_version'].map((slug) => {
                                    return (
                                        <div className={'flex justify-between my-1'} key={slug}>
                                            <div className={'capitalize'}>{this.trans(slug)}</div>
                                            <div className={'capitalize'}>{this.trans(this.state[slug] || '-')}</div>
                                        </div>
                                )
                                })}
                            </div>
                            {this.state.hostname && <div className={'bg-white border font-semibold mt-6 p-2 text-black text-center'}>
                                {this.state.hostname + '.booting.cloud'}
                            </div>}
                        </div>
                    </div> */}
                </div>

            </div>
        );
    }
}

export default App;
