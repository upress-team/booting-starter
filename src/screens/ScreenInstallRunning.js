import React, { Component } from 'react';
import Icon from "@mdi/react";
import {mdiArrowDownBoldHexagonOutline, mdiArrowUpBoldHexagonOutline} from "@mdi/js";
import Loader from "../assets/Loader";

class ScreenInstallRunning extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show_screen: false
        };
    }
    toggleScreen() {
        this.setState({show_screen: !this.state.show_screen})
    }

    render() {

        return  (
            <div className='w-full'>

            <div className='text-center'>
                <div className={'text-4xl title'}>Installation in progress</div>
                <div className='text-gray-500'>Please be patient, it may take a few minutes.</div>
            </div>

                {this.state.show_screen && <pre className={'animated fadeIn h-80 w-full max-w-90vw md:max-w-70vw my-10 px-4 text-xs overflow-x-auto overflow-y-hidden text-gray-300'} id="buffer">{this.props.buffer ? this.props.buffer : 'Waiting for data'}</pre>}
                {!this.state.show_screen && <div className={'h-32 relative'}><Loader/></div>}

            <div className={'flex justify-center'}>
                <button onClick={this.toggleScreen.bind(this)} className={'flex font-semibold p-2 items-center text-gray-500'}><Icon path={this.state.show_screen ? mdiArrowUpBoldHexagonOutline : mdiArrowDownBoldHexagonOutline} size={1} color={'#a0aec0'}/><span className={'mx-2'}>{this.state.show_screen ? 'Hide Details' : 'Show Details'}</span></button>
            </div>

            </div>
        );
    }
}

export default ScreenInstallRunning;
