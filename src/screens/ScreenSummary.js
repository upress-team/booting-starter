import React, { Component } from 'react';
import Icon from '@mdi/react'
import { mdiArrowLeftBoldHexagonOutline } from '@mdi/js'
import { mdiArrowRightBoldHexagonOutline } from '@mdi/js'
import DebounceInput from "react-debounce-input";
import Loader from "../assets/Loader";

class ScreenSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            licensekey: '',
            trial: false
        };
    }

    nextScreen(){
      if (this.props.state && this.props.state.isBusy) return;
      this.props.confirmButtonAction(this.props.state.hideLicense ? this.props.state.hideLicense : (this.state.trial ? 'trial' : this.state.licensekey));
    }
    handleSubmit(e) {
        e.preventDefault();
        this.nextScreen();
    }
    render() {
        // console.log(this.props.state);

        return  (
            <div className=''>

            <div className='text-center'>
                <div className={'text-4xl title'}>Server Summary</div>
                <div className='text-gray-500'>Please make sure all details are correct.</div>
                <div className={'flex my-12'}>

                    <form className='relative' onSubmit={this.handleSubmit.bind(this)}>

                        <p className={'max-w-2xl'}>
                            {this.props.state.server_purpose === 'stand_alone' &&
                                <React.Fragment>
                                    <span className={'summary_span'}>{this.props.state.server_type}</span> <span className={'summary_span'}>stand alone </span> <span>server </span>
                                </React.Fragment>
                            }
                            {this.props.state.server_purpose === 'part_of_array' &&
                                <React.Fragment>
                                    {this.props.state.server_type === 'mysql' && <span className={'summary_span'}>mysql server</span>} as <span className={'summary_span'}>part of array </span>
                                </React.Fragment>
                            }
                            called <span className={'summary_span'}>{this.props.state.hostname}.booting.cloud</span>,
                            <br/>
                            {['mysql', 'nginx', 'apache'].includes(this.props.state.server_type) && <React.Fragment>
                                {this.props.state.mysql_version !== "0" && <React.Fragment>running with MySQL version <span className={'summary_span'}>{this.props.state.mysql_version}</span></React.Fragment>}
                                {this.props.state.mysql_version === "0" && <React.Fragment>running <span className={'summary_span'}>without</span> MySQL </React.Fragment>}
                            </React.Fragment>
                            }
                            {['nginx', 'apache'].includes(this.props.state.server_type) && <React.Fragment> and PHP version <span className={'summary_span'}>{this.props.state.php_version}</span></React.Fragment>}
                            .<br/>
                            Messages will be sent to {/*<span className={'summary_span'}>{this.props.state.username}</span> at */}<span className={'summary_span'}>{this.props.state.email}</span>.
                        </p>
                        <div className='text-gray-500 text-sm mt-4'>You can change the settings at any time in the future.</div>


                        {this.props.state && !this.props.state.hideLicense && <div className={'flex items-center mt-4 w-full'}>
                            <label className="text-left block mx-2 text-sm tracking-wide uppercase w-56 shrink-0">License Key</label>
                            <label className={"flex items-center items-stretch appearance-none border border-gray-200 flex-grow bg-white py-3 px-4 rounded leading-tight focus:outline-none focus:border-gray-500 "  + (this.state.trial ? 'bg-gray-100' : '')}>
                                <input type="text"
                                               autoFocus
                                               className={"min-w-28 leading-tight focus:outline-none"}
                                               placeholder={'licensekey'}
                                               value={this.state.licensekey}
                                               minLength={6}
                                               maxLength={20}
                                               disabled={this.state.trial}
                                               onChange={(e) => this.setState({licensekey: e.target.value})}
                                               onInput={(e) => this.setState({licensekey: e.target.value})}
                                />
                            </label>

                            <div className={'py-3'}>
                                <label className="text-xs text-gray-900 dark:text-gray-300 select-none">
                                    <input type="checkbox" className="w-3 h-3 m-1 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" name="isLocalHostname"  onChange= {() => this.setState({trial: !this.state.trial})} checked={this.state.trial} /> Setup License Later (Trial)
                                </label>
                            </div>

                        </div>}

                        {this.props.state && this.props.state.message && <div className={'text-pink-600 mt-4'}>{this.props.state.message}</div>}

                    </form>

                </div>
            </div>

              <div className={'flex justify-center'}>
                <button onClick={this.props.previusScreen} disabled={this.props.state && this.props.state.isBusy} className={'flex font-semibold p-2 items-center text-gray-500'}><Icon path={mdiArrowLeftBoldHexagonOutline} size={1} color={'#a0aec0'}/><span className={'mx-2'}>Prev</span></button>
                <button disabled={(this.props.state && !this.props.state.hideLicense && this.state.licensekey.length === 0 && !this.state.trial) || (this.props.state && this.props.state.isBusy)} onClick={() => this.nextScreen()} className={'bg-booting-lightblue flex font-semibold items-center p-2 rounded text-white ' + ( (this.props.state && !this.props.state.hideLicense && this.state.licensekey.length === 0 && !this.state.trial) || (this.props.state && this.props.state.isBusy)? 'disabled' : '')}><span className={'mx-2'}>Start Installation</span><Icon path={mdiArrowRightBoldHexagonOutline} size={1} color={'#fff'}/></button>
              </div>

                {this.props.state && this.props.state.isBusy && <div className={'relative h-20'}><Loader/></div>}

            </div>
        );
    }
}

export default ScreenSummary;
