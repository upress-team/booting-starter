import React, { Component } from 'react';
import Icon from '@mdi/react'
import { mdiCardOutline } from '@mdi/js'
import { mdiArrowLeftBoldHexagonOutline } from '@mdi/js'
import Apache from '../assets/svg/Apache';
import Nginx from '../assets/svg/Nginx';
import MySQL from '../assets/svg/MySQL';
import Node from '../assets/svg/Node';

class ScreenSelectServertype extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    getOptions() {
        if (this.props.server_purpose === 'stand_alone') return (
                <React.Fragment>
                    {/*<button className={'btn-big node'} onClick={this.props.confirmButtonAction('nodejs')}>
                        <Node/>
                        Node.js Server
                    </button>*/}
                    <button className={'btn-big nginx'} onClick={this.props.confirmButtonAction('nginx')}>
                        <Nginx/>
                        Nginx Web Server
                    </button>
                    <button className={'btn-big apache'} onClick={this.props.confirmButtonAction('apache')}>
                        <Apache/>
                        Apache Web Server
                    </button>
                </React.Fragment>
            )
            else return (
                <button className={'btn-big mysql'} onClick={this.props.confirmButtonAction('mysql')}>
                    <MySQL/>
                    Mysql Server
                </button>
            )
    }
    render() {
        return  (
            <div className='text-center'>
                <div className={'text-4xl title'}>The purpose of the server</div>
                <div className='text-gray-500'>Is the server a standalone and separate server or is it part of another server system?</div>
                <div className={'flex justify-center my-12'}>
                    {this.getOptions()}
                </div>
                <div className={'flex justify-center'}>
                  <button onClick={this.props.previusScreen} className={'flex p-2 font-semibold items-center text-gray-500'}><Icon path={mdiArrowLeftBoldHexagonOutline} size={1} color={'#a0aec0'}/><span className={'mx-2'}>Prev</span></button>
                </div>
            </div>
        );
    }
}

export default ScreenSelectServertype;
