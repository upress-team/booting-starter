import React, { Component } from 'react';
import Icon from '@mdi/react'
import { mdiCallMerge, mdiWindowShutterOpen, mdiServerPlus, mdiServerNetwork } from '@mdi/js'

class ScreenSelectServerPurpose extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {

        return  (
            <div className='text-center'>
                <div className={'text-4xl title'}>The purpose of the server</div>
                <div className='text-gray-500'>Is the server a standalone and separate server or is it part of another server system?</div>
                <div className={'flex justify-center my-12'}>
                    <button className={'btn-big'} onClick={this.props.confirmButtonAction('stand_alone')}>
                        <div className={'mb-2'}><Icon path={mdiServerPlus} className={'transition-all'} size={2} color={'#8c8c8c'}/></div>
                        Standalone Server
                    </button>
                    <button disabled={true} className={'btn-big disabled'} onClick={this.state.someValidate ? this.props.confirmButtonAction('part_of_array') : () => {}}>
                        <div className={'mb-2'}><Icon path={mdiServerNetwork} className={'transition-all'} size={2} color={"#8c8c8c"}/></div>
                        As a Part of a Server Array
                    </button>
                </div>
            </div>
        );
    }
}

export default ScreenSelectServerPurpose;
