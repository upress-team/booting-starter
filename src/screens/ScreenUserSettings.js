import React, { Component } from 'react';
import Icon from '@mdi/react'
import { mdiArrowLeftBoldHexagonOutline } from '@mdi/js'
import { mdiArrowRightBoldHexagonOutline } from '@mdi/js'

class ScreenUserSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // username: props.username ? props.username : 'admin',
            // password: props.password ? props.password : '',
            // confirm_password: props.confirm_password ? props.confirm_password : '',
            email: props.email ? props.email : ''
        };
    }
    handleChange(e) {
        let regex = {
            email: /[^A-Za-z0-9\.@-_]/g,
            // username: /[^A-Za-z0-9]/g,
            // password: /[^A-Za-z0-9\.@-_$!%*#?&+*]/g,
            // confirm_password: /[^A-Za-z0-9\.@-_$!%*#?&+*]/g
        };
        this.setState({[e.target.name]: e.target.value.trim().replace(regex[e.target.name], '')});
    }
    nextScreen(){

        this.setState({isBusy: true, message: null}, () => {
            if (!/(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g.test(this.state.email) || !this.state.email) return this.setState({
                message: 'Please enter a valid email address'
            });
            // if (!/^([A-Za-z0-9]){4,20}$/g.test(this.state.username) || !this.state.username) return this.setState({
            //     message: 'Please enter a valid username containing 4-20 characters (letters and numbers only)'
            // });
            // if ((this.state.password !== this.state.confirm_password) || (!this.state.password || !this.state.confirm_password) || this.state.password.length < 6) return this.setState({
            //     message: 'Please make sure you write a valid same password twice containing at least 6 characters'
            // });

            this.props.confirmButtonAction({
                // username: this.state.username,
                // password: this.state.password,
                email: this.state.email
            })

        });
    }

    previusScreen() {
        this.props.previusScreen({
            // username: this.state.username,
            // password: this.state.password,
            // confirm_password: this.state.confirm_password,
            email: this.state.email
        })
    }
    handleSubmit(e) {
        e.preventDefault();
        this.nextScreen();
    }
    render() {

        return  (
            <div className=''>

            <div className='text-center'>
                <div className={'text-4xl title'}>Please setup the admin user details</div>
                <div className='text-gray-500'>You can change the settings at any time in the future.</div>
                <div className={'flex my-12'}>

                    <form className='relative' onSubmit={this.handleSubmit.bind(this)}>
                        {/*{this.state.isBusy && <Loader/>}*/}

                        <div className={'flex items-center mb-2 w-full'}>
                            <label className="text-left block mx-2 text-sm tracking-wide uppercase w-56 shrink-0">Admin Email</label>
                            <input type="text" autoFocus={true} className="dir-ltr appearance-none flex-grow border rounded py-3 px-4 leading-tight focus:outline-none focus:shadow" name="email" placeholder="john@gmail.com" onChange={this.handleChange.bind(this)} value={this.state.email} />
                        </div>
                        {/*<div className={'flex items-center mb-2 w-full'}>*/}
                        {/*    <label className="text-left block mx-2 text-sm tracking-wide uppercase w-56 shrink-0">Username</label>*/}
                        {/*    <input type="text" className="dir-ltr appearance-none flex-grow border rounded py-3 px-4 leading-tight focus:outline-none focus:shadow" name="username" placeholder="admin" onChange={this.handleChange.bind(this)} value={this.state.username} />*/}
                        {/*</div>*/}
                        {/*<div className={'flex items-center mb-2 w-full'}>*/}
                        {/*    <label className="text-left block mx-2 text-sm tracking-wide uppercase w-56 shrink-0">Password</label>*/}
                        {/*    <input type="password" className="dir-ltr appearance-none flex-grow border rounded py-3 px-4 leading-tight focus:outline-none focus:shadow" name="password" placeholder="password" onChange={this.handleChange.bind(this)} value={this.state.password} />*/}
                        {/*</div>*/}
                        {/*<div className={'flex items-center mb-2 w-full'}>*/}
                        {/*    <label className="text-left block mx-2 text-sm tracking-wide uppercase w-56 shrink-0">Confirm Password</label>*/}
                        {/*    <input type="password" className="dir-ltr appearance-none flex-grow border rounded py-3 px-4 leading-tight focus:outline-none focus:shadow" name="confirm_password" placeholder="password" onChange={this.handleChange.bind(this)} value={this.state.confirm_password} />*/}
                        {/*</div>*/}

                        {this.state.message && <div className={'max-w-xl text-sm bg-pink-100 text-center text-pink-700 justify-center px-10 flex items-center border border-pink-700 py-2 rounded mx-1 my-3'}>{this.state.message}</div>}

                    </form>

                </div>
            </div>

              <div className={'flex justify-center'}>
                <button onClick={() => this.previusScreen()} className={'flex font-semibold p-2 items-center text-gray-500'}><Icon path={mdiArrowLeftBoldHexagonOutline} size={1} color={'#a0aec0'}/><span className={'mx-2'}>Prev</span></button>
                <button onClick={() => this.nextScreen()} className={'bg-booting-lightblue flex font-semibold items-center p-2 rounded text-white'}><span className={'mx-2'}>Next</span><Icon path={mdiArrowRightBoldHexagonOutline} size={1} color={'#fff'}/></button>
              </div>

            </div>
        );
    }
}

export default ScreenUserSettings;
