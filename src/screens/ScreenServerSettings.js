import React, { Component } from 'react';
import Icon from '@mdi/react'
import { mdiArrowLeftBoldHexagonOutline } from '@mdi/js'
import { mdiArrowRightBoldHexagonOutline } from '@mdi/js'
import DebounceInput from 'react-debounce-input';
import socketIOClient from "socket.io-client";
import Loader from "../assets/Loader";
const isDev = window.location.hostname === 'localhost';
const socketEndpoint =  isDev ? "http://localhost:3000" : "/";

class ScreenServerSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isBusy: false,
            php_version: props.php_version ? props.php_version : "7.4",
            php_versions: [],
            mysql_version: props.mysql_version ? props.mysql_version : "5.7",
            mysql_versions: [],
            hostname: props.hostname ? props.hostname.toLowerCase() : '',
            email: props.email ? props.email : '',
            isValidate: false,
            message: null,
        };
        this.socket = null
    }
    componentDidMount() {
        this.socket = socketIOClient(socketEndpoint);
        if (this.props.isLocalHostname) this.props.handleIsLocalHostnameChange()
        if (this.props.hostname) this.validateHostname();
        this.getServiceOptions();
    }

    handleChange(e) {
        let regex = {
            email: /[^A-Za-z0-9\.@-_]/g,
        };

        if (e.target.name === 'hostname') this.setState({[e.target.name]: e.target.value.trim().replace(/[^A-Za-z0-9-]+/g, '')});
        else this.setState({[e.target.name]: e.target.value.trim().replace(regex[e.target.name], '')});
    }

    getServiceOptions() {
        this.setState({isBusy: true}, () => {
                this.socket.emit('getServiceOptions', (data) => {
                    if (data.success && data.results) {
                        if (data.results.mysqlVersions) this.setState({mysql_versions: data.results.mysqlVersions});
                        if (data.results.phpVersions) this.setState({php_versions: data.results.phpVersions});
                        this.setState({isBusy: false})
                    }
                });
        })
    }

    isLocalHostnameChange(e) {
        this.props.handleIsLocalHostnameChange();
        this.validateHostname();
    }
    validateHostname() {
        if (this.props.state && this.props.state.hideLicense) return this.setState({isValidate: true, validating: false});
        if (!this.state.hostname || this.state.hostname.length < 2) return;

        this.setState({isValidate: false, message: null, validating: true}, () => {
            const hostname = this.state.hostname;
            if ( !/^(?!:\/\/)([a-zA-Z0-9-*]+\.)*[a-zA-Z0-9][a-zA-Z0-9-]+\.[a-zA-Z]{2,11}?$/.test(hostname ? hostname.toLowerCase() : '') ) {
                this.setState({message: 'please enter a valid hostname', validating: false})
            } else if (this.props.isLocalHostname) {
                this.setState({isValidate: true, validating: false})
            } else {
                // console.log('hostname',hostname);
                this.socket.emit('validateHostname', (hostname ? hostname.toLowerCase() : '') , (data) => {
                    if (data.success) {
                        if (!data.output) {
                            this.setState({isValidate: true, validating: false})
                        } else {
                            const hostnameValidationErrMsg = typeof(data.output) == 'string' ? data.output
                            :'Hostname is not valid or currently in use, please select another hostname';
                            this.setState({message: hostnameValidationErrMsg, validating: false})
                        }
                    }
                });
            }
        })
    }
    nextScreen(e){
        if(e) e.preventDefault();
        if (!this.state.isValidate) return;
        this.setState({message: null}, () => {
            if(this.props.state && !this.props.state.hideLicense && (!/^([A-Za-z0-9-_.]){6,40}$/g.test(this.state.hostname) && !this.props.isLocalHostname|| !this.state.hostname)) return this.setState({message: 'Please enter a hostname containing 6-20 characters (letters and numbers only)'});
            if (!/(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g.test(this.state.email) || !this.state.email) return this.setState({

                message: 'Please enter a valid email address'
            });

            this.props.confirmButtonAction({
                php_version: this.state.php_version,
                mysql_version: this.state.mysql_version,
                hostname: this.state.hostname.toLowerCase(),
                email: this.state.email.toLowerCase()
            })
        })
    }
    previusScreen() {
        this.props.previusScreen({
            php_version: this.state.php_version,
            mysql_version: this.state.mysql_version,
            hostname: this.state.hostname.toLowerCase(),
            email: this.state.email.toLowerCase()
        })
    }
    handleSubmit(e) {
        e.preventDefault();
        this.nextScreen();
    }
    render() {

        return  (
            <div className=''>

            <div className='text-center'>
                <div className={'text-4xl title'}>Please select the initial server configuration</div>
                <div className='text-gray-500'>You can change the settings at any time in the future.</div>
                <div className={'flex my-12'}>

                    <form className='relative' onSubmit={this.handleSubmit.bind(this)}>
                        {this.state.isBusy && <Loader/>}


                        {this.props.state && !this.props.state.hideLicense && <div className={'flex items-center mb-2 w-full'}>
                            <label className="text-left block mx-2 text-sm tracking-wide uppercase w-56 shrink-0">Hostname</label>

                            <label className={"flex items-center items-stretch appearance-none border border-gray-200 flex-grow bg-white py-3 mb-3 px-4 rounded leading-tight focus:outline-none focus:border-gray-500 " + (this.state.isValidate ? 'border-green-300' : 'border-red-300')}>

                                <DebounceInput type="text"
                                               autoFocus
                                               style={{width: (this.state.hostname.length * 0.452 ) + 'rem'}}
                                               className="min-w-28 leading-tight focus:outline-none"
                                               placeholder={'hostname'}
                                               value={this.state.hostname ? this.state.hostname.toLowerCase() : ''}
                                               minLength={1}
                                               maxLength={40}
                                               debounceTimeout={500}
                                               onChange={this.validateHostname.bind(this)}
                                               onInput={e => {
                                                   const value = e.target.value || '';
                                                   this.setState({isValidate: false}, this.setState({ hostname: value ? value.trim().toLowerCase() : '' }))
                                               }}
                                />
                                <span className="flex-none text-dusty-blue-darker select-none leading-none"></span>
                            </label>

                            <div className={'py-3 mb-3'}>
                                <label className="text-xs text-gray-900 dark:text-gray-300 select-none">
                                    <input type="checkbox" className="w-3 h-3 m-1 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" name="isLocalHostname"  onChange= {() => this.isLocalHostnameChange()} value='' /> Skip DNS Validation
                                </label>
                            </div>
                            <div className={'flex items-left mb-2'}>
                             </div>
                        </div>}
                        {['nginx', 'apache'].includes(this.props.server_type) && <div className={'flex items-center mb-2 w-full'}>
                            <label className="text-left block mx-2 text-sm tracking-wide uppercase w-56 shrink-0">PHP Version</label>
                            {/*yum -y search php-fpm 2>&1*/}
                            <select
                                value={this.state.php_version}
                                onChange={this.handleChange.bind(this)}
                                name='php_version'
                                className={"block appearance-none border border-gray-200 bg-white py-3 mb-3 px-4 rounded leading-tight focus:outline-none focus:border-gray-500 flex-grow"}>
                                {this.state.php_versions.map(item => {
                                    return (
                                        <option key={item.value} value={item.value}>{item.label}</option>
                                    )})
                                }
                                            {/*<option value={"5.6"}>PHP 5.6</option>*/}
                                            {/*<option value={"7.0"}>PHP 7.0</option>*/}
                                            {/*<option value={"7.1"}>PHP 7.1</option>*/}
                                            {/*<option value={"7.2"}>PHP 7.2</option>*/}
                                            {/*<option value={"7.3"}>PHP 7.3</option>*/}
                                            {/*<option value={"7.4"}>PHP 7.4</option>*/}
                                            {/*<option value={"8.0"}>PHP 8.0</option>*/}
                            </select>
                        </div>}
                        {['nginx', 'apache', 'mysql'].includes(this.props.server_type) && <div className={'flex items-center mb-2 w-full'}>
                            <label className="text-left block mx-2 text-sm tracking-wide uppercase w-56 shrink-0">MySQL Version</label>
                            <select
                                value={this.state.mysql_version}
                                onChange={this.handleChange.bind(this)}
                                name='mysql_version'
                                className={"block appearance-none border border-gray-200 bg-white py-3 mb-3 px-4 rounded leading-tight focus:outline-none focus:border-gray-500 flex-grow"}>
                                {this.props.server_purpose !== "part_of_array" && <option value={0}>Without MySQL</option>}
                                {this.state.mysql_versions.map(item => {
                                    return (
                                        <option key={item.value} value={item.value}>{item.label}</option>
                                    )})
                                }
                                {/*<option value={"5.6"}>MySQL 5.6</option>*/}
                                {/*<option value={"5.7"}>MySQL 5.7</option>*/}
                                {/*<option value={"8.0"}>MySQL 8.0</option>*/}
                            </select>
                        </div>}

                        <div className={'flex items-center mb-2 w-full'}>
                            <label className="text-left block mx-2 text-sm tracking-wide uppercase w-56 shrink-0">Admin Email</label>
                            <input type="text" className="dir-ltr appearance-none flex-grow border rounded py-3 px-4 leading-tight focus:outline-none focus:shadow" name="email" placeholder="john@gmail.com" onChange={this.handleChange.bind(this)} value={this.state.email} />
                        </div>

                        {this.state.message && <div className={'max-w-xl text-sm bg-pink-100 text-center text-pink-700 justify-center px-10 flex items-center border border-pink-700 py-2 rounded mx-1 my-3'}>{this.state.message}</div>}

                    </form>

                </div>
            </div>

            <div className={'flex justify-center'}>
              <button onClick={() => this.previusScreen()} className={'flex font-semibold p-2 items-center text-gray-500'}><Icon path={mdiArrowLeftBoldHexagonOutline} size={1} color={'#a0aec0'}/><span className={'mx-2'}>Prev</span></button>
                <button onClick={() => this.nextScreen()} disabled={!this.state.isValidate || this.state.validating || !this.state.php_version || !/(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g.test(this.state.email)} className={'bg-booting-lightblue flex font-semibold items-center p-2 rounded text-white ' + (this.state.isValidate && /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g.test(this.state.email) ? '' : 'disabled cursor-not-allowed')}><span className={'mx-2'}>Next</span><Icon path={mdiArrowRightBoldHexagonOutline} size={1} color={'#fff'}/></button>
            </div>

            {this.state.validating && <div className={'h-24 relative'}><Loader/></div>}

            </div>
        );
    }
}

export default ScreenServerSettings;
