import React, { Component } from 'react';
import Icon from '@mdi/react'
import {mdiArrowRightBoldHexagonOutline, mdiCardOutline, mdiEyeOffOutline, mdiEyeOutline} from '@mdi/js'
import { mdiArrowLeftBoldHexagonOutline } from '@mdi/js'
import Apache from '../assets/svg/Apache';
import Nginx from '../assets/svg/Nginx';
import MySQL from '../assets/svg/MySQL';
import Node from '../assets/svg/Node';
import socketIOClient from "socket.io-client";
const isDev = window.location.hostname === 'localhost';
const socketEndpoint =  isDev ? "http://localhost:3000" : "/";

class ScreenResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalResetPasswordFieldPassword: '',
            error: null
        };
        this.socket = null
    }
    resetUserPassword(e){
        this.socket = socketIOClient(socketEndpoint);
        if(e) e.preventDefault();
        this.setState({ error: null})
        if (!this.state.modalResetPasswordFieldPassword.match(/^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/g)) return this.setState({ error: '`Please enter a password at least 8 character and contain At least one uppercase.At least one lower case.At least one special character.`'});
        this.setState({isBusy: true}, () => {
            this.socket.emit('resetPassword', this.state.modalResetPasswordFieldPassword, (data) => {
                if (data.success) {
                    // console.log('11111121212');
                    // this.setState({isValidate: true, validating: false});
                    this.props.confirmButtonAction();
                } else {
                    this.setState({error: 'please enter a valid password'})
                }
            });
            /*Users.resetUserPassword(this.props.user.username, this.state.modalResetPasswordFieldPassword).then((response) => {
                this.setState({isBusy: false});
                // console.log(response.data);
                this.props.getUserData();
                toast(this.props.gettext, this.props.gettext(`password_reset_successfully`), 'success', false, 3000);
            }).catch((response) => {
                this.setState({isBusy: false});
                handleError(response);
            })*/
        })
    }
    handleChange(e) {
        let target = e.target.name;
        let value = e.target.value;
        // if (target === 'modalResetPasswordFieldPassword') this.setState({score: zxcvbn(value).score});
        this.setState({[target]: value});
    }
    render() {

        return  (
            <div className='text-center'>
                <div className={'text-4xl title'}>Reset the default root password</div>
                <div className='text-gray-500'>To reset the current password, please enter a new password below</div>
                <div className={'block mt-2 text-yellow-700 capitalize-first'}><b>password requirements:</b> minimum of eight characters, one uppercase letter, one lowercase letter, one number and one special character.</div>

                <div className={'flex justify-center my-12'}>
                    <div className="leading-loose">
                        <form className="" onSubmit={this.resetUserPassword.bind(this)}>
                            <div className="">
                                {/*<label className="block text-sm text-gray-700" htmlFor="modalResetPasswordFieldPassword">Enter Password</label>*/}
                                <div className={'flex'}>
                                    <input className="px-5 w-full py-4 text-gray-700 bg-gray-200 rounded ltr rtl:text-right" disabled={this.props.isBusy}
                                           onChange={this.handleChange.bind(this)} onInput={this.handleChange.bind(this)} value={this.state.modalResetPasswordFieldPassword} autoFocus={true}
                                           name="modalResetPasswordFieldPassword" type={this.state.passwordToggleVisibility ? 'text' : "password"} required={true}
                                           placeholder={'enter password'} aria-label="modalResetPasswordFieldPassword" />
                                    <button type="button" className={'bg-gray-400 px-3'} onClick={() => this.setState({passwordToggleVisibility: !this.state.passwordToggleVisibility})}><Icon color={'#4a5568'} path={!this.state.passwordToggleVisibility ? mdiEyeOutline : mdiEyeOffOutline} size={0.8}/></button>
                                </div>
                                <div className={'h-2 mt-1 ' + (this.state.score === 1 ? 'w-1/4 bg-pink-400' : '') + (this.state.score === 2 ? 'w-1/2 bg-yellow-400' : '') + (this.state.score > 2 && !this.state.modalResetPasswordFieldPassword.match(/^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/g) ? 'w-3/4 bg-blue-400' : '') + (this.state.score > 2 && this.state.modalResetPasswordFieldPassword.match(/^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/g) ? 'w-full bg-green-400' : '')} />
                            </div>
                        </form>
                        {this.state.error && <div className={'max-w-xl text-sm bg-pink-100 text-center text-pink-700 justify-center px-10 flex items-center border border-pink-700 py-2 rounded mx-1 my-3'}>{this.state.error}</div>}
                    </div>
                </div>

                <div className={'flex justify-center'}>
                  <button onClick={() => this.resetUserPassword()} className={'flex p-2 font-semibold items-center text-gray-500'}><Icon path={mdiArrowRightBoldHexagonOutline} size={1} color={'#a0aec0'}/><span className={'mx-2'}>Reset Password</span></button>
                </div>
            </div>
        );
    }
}

export default ScreenResetPassword;
