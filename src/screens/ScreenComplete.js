import React, { Component } from 'react';
import Icon from '@mdi/react'
import { mdiArrowLeftBoldHexagonOutline } from '@mdi/js'
import { mdiArrowRightBoldHexagonOutline } from '@mdi/js'

class ScreenComplete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hostnameRef: this.props.isLocalHostname ? window.location.hostname : (this.props.hostname || window.location.hostname)
        };
    }

    render() {
        return  (
            <div className=''>

            <div className='text-center'>
                <div className={'text-4xl title'}>The server was successfully installed</div>
                <div className='text-gray-500'>Please continue the server setup process</div>
                <div className={'flex justify-center my-12'}>

                    <p className={'max-w-2xl'}>
                        The initial setup process has been completed successfully, you can now open the server management panel and continue to configure your server in an advanced and convenient way.
                    </p>

                </div>
            </div>

              <div className={'flex justify-center'}>
                <a href={'https://' + this.state.hostnameRef + ':8443'} className={'bg-booting-lightblue flex font-semibold items-center p-2 rounded text-white'} onClick={() => {
                    this.props.socket.emit('closeInstallation', true);
                    this.props.socket.close();
                }}><span className={'mx-2'}>Continue</span><Icon path={mdiArrowRightBoldHexagonOutline} size={1} color={'#fff'}/></a>
              </div>

            </div>
        );
    }
}

export default ScreenComplete;
