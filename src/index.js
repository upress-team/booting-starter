import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './assets/serviceWorker';

ReactDOM.render(<App />, document.getElementById('rootapp'));
serviceWorker.unregister();