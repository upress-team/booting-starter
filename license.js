const fs = require('fs');
const crypto = require('crypto')
const path = require('path');
const axios = require('axios');
const qs = require('qs');
const fse = require('fs-extra');

// let serverData = server_data.getServerData();
// const { encrypt, decrypt } = require('../utilities/encryption');

function checkLicense (licenseKey, hostname, ip, publicIp = '') {
    return new Promise((async (resolve, reject) => {
        if (!licenseKey) return reject();
        // const ip = '4.4.4.4';
        let licensePath = path.join('/', 'usr', 'local', 'lib', 'booting-agent', 'license.key');

        let response;

        response = await get_remote_license(licenseKey, hostname, ip, publicIp);
        // console.log('response after get_remote_license', response)
        if (response && response.status === 'Active') {
            try {
                fse.outputFileSync(licensePath, response.localKey);
            }
            catch (err) {
                reject(err);
            }
        }

        // console.log('remote response:', response);
        resolve(response);
    }));
}


function parse_license(keyData, hostname, ip, licensekey) {
    // console.log('parse_license', { keyData, hostname, ip, licensekey } )
    // The number of days to wait between performing remote license checks
    const localkeydays = 1;

    // The public key that matches the private key we sign the data with
    const publicKey = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuncZgGrQEmUoEfwIFQkh
FMegY2xt1D7K7VNNfvurJfKmF4xagqu/1xI/emBINyqgGX9EK7oL5vG3ZYS0KE2j
sOrKvHVkaVegnjhktiuD4Oj8q+a5dK8xCNMe8A3G8gooqha6hOA7UgUTs/Y6/GJy
MlvOpDyjvXuXour1q9wUgGDw33sJCR8l1xL3ZavhHajbW8GHWqbUL88WiRvuiMz5
uMMQbHHCfeGGVCpaxETHahvX4S3s4QYRLobHEGvvQr/EK09d9ImoEILmyWcVOzsV
ilDKsJPVxLLqIR1kXdxgL3OMO6ELbcjpQIWRaNQEuOb391AfTtFtNMWY3Lzck+Yi
eQIDAQAB
-----END PUBLIC KEY-----`;

    // Should we check the hostname
    const validatedomain = false;
    // Should we check the IP
    const validateip = true;
    // Should we check the install directory
    const validatedir = false;
    // Should we check the secret algo
    const validateSecret = !!licensekey; //:TODO enable this
    // const validateSecret = false;



    // -----------------------------------
    //  -- Do not edit below this line --
    // -----------------------------------
    const dirpath = path.dirname(require.main.filename);
    /*const dateToYmd = (date) => {
        return date.getFullYear().toString() + (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1).toString() + (date.getDate() < 10 ? '0' : '') + date.getDate().toString();
    }*/
    const dateToYmd = (date) => {
        let dateUTC = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()));
        return dateUTC.getFullYear().toString() + (dateUTC.getMonth() < 9 ? '0' : '') + (dateUTC.getMonth() + 1).toString() + (dateUTC.getDate() < 10 ? '0' : '') + dateUTC.getDate().toString();
    };
    const verifySignature = (data, signature, publicKey) => {
        const verifier = crypto.createVerify('RSA-SHA256')
        verifier.update(data, 'ascii')
        const publicKeyBuf = Buffer.from(publicKey, 'ascii')
        const signatureBuf = Buffer.from(signature, 'hex')

        try {
            return verifier.verify(publicKeyBuf, signatureBuf)
        } catch(e) { /* do nothing */ }

        return false;
    };
    // console.log(22222222222222, keyData)

    const [data, signature] = keyData.replace(/[\r\n]/g, '').split('.');

    // console.log(111, data)
    // console.log(333, signature)

    if(!verifySignature(data, Buffer.from(signature, 'base64'), publicKey)) {
        return {
            status: 'Invalid',
            description: 'Checksum Verification Failed'
        };
    }

    keyData = JSON.parse(Buffer.from(data, 'base64').toString());

    const today = new Date();
    let localexpiry = new Date();
    localexpiry.setDate(today.getDate() - localkeydays)

    // check if the date of the data is within the allow period
    /*console.log('keyData.checkdate', keyData.checkdate);
    console.log('dateToYmd(localexpiry)', dateToYmd(localexpiry));
    console.log('dateToYmd(today)', dateToYmd(today));*/

    /*if(keyData.checkdate <= dateToYmd(localexpiry) || keyData.checkdate > dateToYmd(today)) {
        keyData.status = 'Invalid';
        keyData.description = 'Data Expired';

        return keyData;
    }*/

    const validdomains = keyData && keyData.validdomain && keyData.validdomain.split(',') ? keyData.validdomain.split(',') : [];
    const validips = keyData && keyData.validip && keyData.validip.split(',') ? keyData.validip.split(',') : [];
    // console.log('keyData.validip', keyData.validip);
    const validdirs = keyData && keyData.validdirectory && keyData.validdirectory.split(',') ? keyData.validdirectory.split(',') : [];
    const validsecret = keyData.secret;

    if (validatedomain && !validdomains.includes(hostname)) {
        keyData.status = 'Invalid';
        keyData.description = 'Invalid domain';
    }
    if (validateip && !validips.includes(ip)) {
        keyData.status = 'Invalid';
        keyData.description = 'Invalid IP';
    }
    if (validatedir && !validdirs.includes(dirpath)) {
        keyData.status = 'Invalid';
        keyData.description = 'Invalid directory';
    }
    if (validateSecret && validsecret !== Math.ceil((parseInt(licensekey.replace(/\D+/g, '')) / parseInt('198564')) * 0.246798)) {
        keyData.status = 'Invalid';
        keyData.description = 'Invalid License';
        keyData.secret = '';
    }

    return keyData;
}

async function get_remote_license(licensekey, hostname, iplocal, publicIp) {
    // let ippublic = await publicIp.v4();

    // console.log('get_remote_license');
    // Enter the url to your WHMCS installation here
    const whmcsurl = 'https://license.booting.me/';

    const dirpath = path.dirname(require.main.filename);
    const verifyfilepath = 'modules/addons/booting/verify.php';

    let responseCode = 0;
    const postfields = {
        licensekey: licensekey,
        domain: hostname,
        ip: [iplocal, publicIp].join(';'),
        dir: dirpath
    };

    // console.log(postfields)
    const query_string = qs.stringify(postfields);
    let axios_response;
    try {
        axios_response = await axios.post(whmcsurl + verifyfilepath, query_string);
    } catch (err) {
        console.log('error on response')
        return {
            status: 'Invalid',
            description: err.message || err
        };
    }
    const data = axios_response.data;
    // console.log(111, data)
    responseCode = axios_response.status;

    if(responseCode !== 200) {
        return {
            status: 'Invalid',
            description: 'Remote Check Failed'
        };
    }

    if(!data) {
        return {
            status: 'Invalid',
            description: 'Invalid License Server Response'
        };
    }

    let results = {};
    if(/<(.*?)>([^<]+)?<\/\1>/gi.test(data)) {
        const matches = data.matchAll(/<(.*?)>([^<]+)?<\/\1>/gi);
        for(item of matches) {
            results[item[1]] = item[2];
        }
        return results;
    }
    results = parse_license(data, hostname, [iplocal, publicIp].join(';'), licensekey);
    // console.log(results)

    results.localKey = results.secret ? data : '';
    results.remotecheck = true;
    return results;
}
module.exports.checkLicense = checkLicense;
